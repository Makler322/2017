my_file = open('diary.txt', 'r')
output = open('diary1.txt', 'w')
a = 0
n = int(my_file.readline())
k, m1, m2 = my_file.readline().split()
k = int(k)
m1 = int(m1)
m2 = int(m2)
for i in range(n):
    S = my_file.readline().split()
    h, t = int(S[0]), int(S[1])
    size = list(map(int, S[2:]))
    for j in range(t):
        if min(m1 * size[j], h * k) - max(m2 * size[j], h) < 0:
            a+=1
output.write(str(a))