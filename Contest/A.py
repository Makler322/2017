n = int(input())
import math
if n == 1:
    print(1)
    exit()
if n % 2 != 0:
    print(n * (n - 1) * (n - 2))
    exit(0)
else:
    if n == 1:
        print(1)
        exit(0)
    elif n == 2:
        print(2)
        exit(0)
    elif n == 3:
        print(6)
        exit(0)
    elif n == 4:
        print(12)
        exit(0)

    else:
        for i in range(n - 2, -1, -1):
            if math.gcd(i, n) == 1 and math.gcd(i, n - 1) == 1:
                if (n * (n - 1) * i) < (((n - 1) * (n - 2) * (n - 3))):
                    print(((n - 1) * (n - 2) * (n - 3)))
                    exit(0)
                else:
                    print(n * (n - 1) * i)
                    exit(0)