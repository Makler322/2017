n , k = map(int, input().split())
import time

import collections as coll

S = coll.deque()
k1 = 0
for i in range(int(n**0.5+1e-12), 0 , -1):
    if n % i == 0:
        if i != n // i:
            S.appendleft(i)

        S.append(n//i)

if len(S) < k:
    print(-1)
else:
    print(S[k - 1])