a,b,c=map(int,input().split())
k=0
if a+b<c:
    print('Impossible')
elif a+b!=0:
    k1=a/(a+b)
    k2=b/(a+b)
    p1=c*k1
    p2=c*k2
    if p1==p2:
        print(round(p1),c-round(p1))
    else:
        print(round(p1),round(p2))
elif c!=0:
    print('Impossible')
else:
    print(0,0)