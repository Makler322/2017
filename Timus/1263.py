N,M=map(int,input().split())
S=[0]*(N+1)
for i in range(M):
    a=int(input())
    S[a]+=1
for i in range(1,len(S)):
    print('{0:.2f}%'.format(int(S[i])*100/M))
