n = int(input())
S = list(map(int,input().split()))
k = 0
for el in S:
    k += (el - 1) // 2
if k % 2 == 0:
    print('Stannis')
else:
    print('Daenerys')