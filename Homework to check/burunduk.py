import matplotlib.pyplot as plt

N=10 #Численность белок
M=20 #Численность бурундуков
Ln=70 #Максимальная численность белок
Lm=50 #Максимальная численность бурундуков
Kn=0.7 #Коэффициент прироста белок
Km=0.7 #Коэффициент прироста бурундуков
Dn=0.1 #Коэффициент взаимного влияния белок
Dm=0.1 #Коэффициент взаимного влияния бурундуков
Time=25 #Промежуток времени за который мы рассматриваем изменение популяций
X=[0] #Массив времнеи
Yn=[N] #Массив изменения популяции белок
Ym=[M] #Массив изменения популяции бурундуков

def euler(N,M,Ln,Lm,Kn,Km,Dn,Dm): #Функция Эйлера
    Ni=(1+Kn*((Ln-N)/Ln))*N-Dn*M #Формула для изменения численности белок
    Mi=(1+Km*((Lm-M)/Lm))*M-Dm*N #Формула для изменения численности бурундуков
    return Ni,Mi


for i in range(1,Time+1): #Основная программа, которая используя фуункцию Эйлера обновляет массивы Yn и Ym
    N,M=euler(N,M,Ln,Lm,Kn,Km,Dn,Dm)
    X.append(i)
    Yn.append(N)
    Ym.append(M)

for i in range(len(Yn)-1): #Ответ на вопрос, сколько белок и бурундуков будет в состоянии равновесия
    if int(Yn[i])==int(Yn[i+1]) and int(Ym[i])==int(Ym[i+1]):
        print(int(Yn[i]),int(Ym[i]))
        break
Dm=0
X1=[]
Y1=[]
while 1!=0: #Ответ на вопрос, при каком Dm бурундуки вымрут через 25 лет.
    Dm+=0.00001
    N=10
    M=20
    X1.append(Dm)
    for i in range(1,Time+1):
        N, M = euler(N, M, Ln, Lm, Kn, Km, Dn, Dm)
    if M<0.001:
        Y1.append(M)
        break
    else:
        Y1.append(M)
print(X1[-1])
graph = plt.figure() #Графический вывод
ax = graph.add_subplot(111)
plt.xlabel('Year')
plt.ylabel('Blue - Squirrel, Red - Chipmunk')
ax.plot(X,Yn,'b')
ax.plot(X,Ym,'r')
#ax.plot(X1,Y1) #Графический ответ на 2 вопрос
plt.show()

