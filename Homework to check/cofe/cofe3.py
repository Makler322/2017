import matplotlib.pyplot as plt


r=0.041 #Коэффициент r, посчитанный нами ранее
Tk=22 #Комнатная температура
Tstart=90  #Начальная температура кофе
dT=1/1000 #Шаг времени
Tend=75 #Конечная температура кофе
milk=5 #Температура на которую молоко охладит кофе
n=Tend+milk #Температура от которой мы будем добавлять молоко
X = [] #Массив температуры
Y = [] #Массив времени
X75=[]
Y75=[]
X90=[]
Y90=[]


def euler(Tnew): #Функция Эйлера
    Tnew=Tnew-r*(Tnew-Tk)*dT
    return Tnew

'''
while n<=Tstart:
    Tnew=90  #Начальная температура кофе, анулирование каждый раз
    n+=0.01 #Шаг с которым идёт изменение температры
    k=0 #Итоговое время, с каждой температурой идёт анулирование
    while euler(Tnew)>=n: #Сначала идём Эйлером до теспературы n
        Tnew=euler(Tnew)
        k+=dT
    Tnew-=milk #Потом добавляем молоко
    while euler(Tnew)>=Tend: #Потом идём Элером до конечной температуры
        Tnew=euler(Tnew)
        k+=dT
    X.append(n) #Добавляем в массивы температуру и время
    Y.append(k)
'''
k=0
Tnew=90
while euler(Tnew) >= 80:  # Сначала идём Эйлером до теспературы n
    Tnew = euler(Tnew)
    k += dT
    X75.append(k)
    Y75.append(Tnew)

Tnew=85
k=0
while euler(Tnew) >= 75:  # Потом идём Элером до конечной температуры
    Tnew = euler(Tnew)
    k += dT
    X90.append(k)  # Добавляем в массивы температуру и время
    Y90.append(Tnew)



graph = plt.figure()  #Графический вывод
ax = graph.add_subplot(111)
plt.ylabel('blue-(85-75), red-(90-80) ')
plt.xlabel('Cooling time, minutes')
#ax.plot(X,Y, 'b')
#plt.show()
ax.plot(X75,Y75, 'r')
ax.plot(X90,Y90, 'b')
plt.show()