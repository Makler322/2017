def function(x): # Функция y(x), по х возвращает у
    return x**2

def pr(x): # Функция возращающая производную от х**2
    return 2*x

def euler(x,y,step): #Алгоритм Эйлера
    x+=step
    y+=pr(x)*step
    return x,y
x=1
y=1
k=2
#step=0.1 # Выбираем нужным нам шаг
step=0.01

while x<k:
    x,y=euler(x,y,step)[0],euler(x,y,step)[1]
    okr=8  # Округление до okr знака
    print(round(x,okr), round(y,okr), round(function(x),okr))