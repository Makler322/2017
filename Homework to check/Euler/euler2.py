def function(x): # Функция y(x), по х возвращает у
    return 3*(x**2)+5*x-2

def pr(x): # Функция возращающая производную
    return 6*x+5

def euler(x,y,step): #Алгоритм Эйлера
    x+=step
    y+=pr(x)*step
    return x,y
x=0
y=-2
k=5
#step=0.1 # Выбираем нужным нам шаг
#step=0.01
step=0.001
while x<k:
    x,y=euler(x,y,step)[0],euler(x,y,step)[1]
    okr=8  # Округление до okr знака
    print(round(x,okr), round(y,okr), round(function(x),okr))