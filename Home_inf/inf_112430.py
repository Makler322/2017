n = int(input())
S = []
D = []
if n < 3:
    print(0)
    exit()
for i in range(n):
    x, y = map(int, input().split())
    if y == 0:
        S.append(x)
    else:
        D.append(abs(y))
if len(S) == 0 or len(D) == 0:
    print(0)
    exit()

ab = max(S) - min(S)
h = max(D)
print((ab * h) / 2)