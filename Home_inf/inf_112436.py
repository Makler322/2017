n = int(input())
d = {}
maxx = 0
B = [0] * 105
C = []

for i in range(n):
    A = input().split()
    ball = int(A[-1])
    scholl = int(A[-2])
    if maxx < ball:
        maxx = ball
        B = [0] * 105
    if ball == maxx:
        B[scholl] += 1
c = max(B)
for i in range(len(B)):
    if B[i] == c:
        print(i, end = ' ')