N = int(input())
S = []
for i in range(N):
    a = float(input())
    if a > 1:
        print(i + 1, end = ' ')
    else:
        S.append([i + 1, a])

if len(S) == N:
    maxx = 0
    for i in S:
        if i[1] > maxx:
            maxx = i[1]
            b = i[0]
    print(b)