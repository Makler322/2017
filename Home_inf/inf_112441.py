n = int(input())
S = []
D = []
for i in range(n):
    a = int(input())
    if a % 2 == 0:
        D.append(a)
    else:
        S.append(a)
if len(S) == 1 and len(D) == 1:
    print(S[0] + D[0])
    exit()

if len(S) < 2:
    mc = min(D)
    D.remove(mc)
    md = min(D)
    print(mc + md)
    exit()
else:
    c = min(S)
    S.remove(c)
    d = min(S)

if len(D) < 2:
    print(c + d)
    exit()
else:
    mc = min(D)
    D.remove(mc)
    md = min(D)

print(min(mc + md, c + d))