n = int(input())
S15 = []
S20 = []
S25 = []
D = []
for i in range(n):
    A = input().split()
    if int(A[-2]) == 15:
        S15.append(A[-1])
    elif int(A[-2]) == 20:
        S20.append(A[-1])
    elif int(A[-2]) == 25:
        S25.append(A[-1])
if len(S15) == 0:
    print(0, end = ' ')
else:
    c15 = min(S15)
    print(S15.count(c15), end = ' ')

if len(S20) == 0:
    print(0, end = ' ')
else:
    c20 = min(S20)
    print(S20.count(c20), end = ' ')

if len(S25) == 0:
    print(0, end = ' ')
else:
    c25 = min(S25)
    print(S25.count(c25), end = '')
