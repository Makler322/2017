n = int(input())
d = {}
for i in range(1, n + 1):
    A = input().split()
    B = int(A[0])
    C = A[1]
    if C not in d:
        d[C] = [B, i]
    else:
        if d[C][0] < B:
            d[C] = [B, i]
F = sorted(d.items(), key = lambda item: (-item[1][0], item[1][1]))
print('1 place. ', str(F[0][0]), '(', str(F[0][1][0]), ')', sep = '')
print('2 place. ', str(F[1][0]), '(', str(F[1][1][0]), ')', sep = '')
print('3 place. ', str(F[2][0]), '(', str(F[2][1][0]), ')', sep = '')