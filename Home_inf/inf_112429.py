import datetime
n = int(input())
S = []
T = []
for i in range(n):
    A = input().split()
    d, m, y = map(int, A[2].split('.'))
    #print(d, m, y)
    T.append(y * 2000 + m * 50 + d)
    S.append(A)
c = min(T)
if T.count(c) > 1:
    print(T.count(c))
else:
    for i in range(len(S)):
        if T[i] == c:
            print(' '.join(map(str, S[i])))
            exit()