'''
    class Weather_station():  # Метеостанция
        def humidity_sensor(self):  # Датчик влажности

        def temperature_sensor(self):  # Датчик температуры

        def pressure_sensor(self):  # Датчик давления
'''
class Subject():
    def register_observer(self):  # Зарегистрировать объект
        raise NotImplementedError()

    def remove_observer(self):  # Удалить объект
        raise NotImplementedError()

    def notify_observer(self):  # Оповестить об показании температуры
        raise NotImplementedError()



class WeatherData(Subject):
    def __init__(self):
        self.observers = []  # Создаем массив элементов
        self.s = None

    def register_observer(self, ob):  # Сверяем элемент по типу, если сходится, то добавляем
        if isinstance(ob, Observer):
            self.observers.append(ob)
        else:
            raise TypeError("{0} not is instance Observer".format(type(ob)))

    def remove_observer(self, ob):  # Удаляет объект
        self.observers.remove(ob)

    def notify_observer(self):  #
        for el in self.observers:
            el.update(self.s)
    '''
    def getTemperature(self):

    def getHumidity(self):

    def getPressure(self):
    '''

    '''
        Temperature = getTemperature()
        Humidity = getHumidity()
        Pressure = getPressure()
    '''
    def set_measurements(self, Temperature, Humidity, Pressure):
        self.Temperature = Temperature
        self.Humidity = Humidity
        self.Pressure = Pressure
        self.notify_observer()

    '''
        currentConditionsDisplay(Temperature, Humidity, Pressure)  # Обновляем экран текущего состояния
        statisticsDisplay(Temperature, Humidity, Pressure)  # Обновляем экран статистики
        forestcastDisplay(Temperature, Humidity, Pressure)  # Обновляем экран прогноза
    '''

class Observer:  # Абстракция
    def __init__(self):
        self.Temperature = None
        self.Humidity = None
        self.Pressure = None

    def update(self, Temperature, Humidity, Pressure):
        self.Temperature = Temperature
        self.Humidity = Humidity
        self.Pressure = Pressure


class currentConditionsDisplay(Observer):
    def display(self):  # Обновляем экран текущего состояния
        print(self.Temperature, self.Humidity, self.Pressure)

class statisticsDisplay(Observer):
    def __init__(self):
        super(statisticsDisplay, self).__init__()
        self.Temperatures = []
        self.Humiditys = []
        self.Pressures = []
    def display(self, Temperature, Humidity, Pressure):  # Обновляем экран статистики
        k = 0
        self.Temperatures.append(self.Temperature)
        self.Humiditys.append(self.Humidity)
        self.Pressures.append(self.Pressure)

        for obs in self.Temperatures:
            k += obs

        print(k/len(self.Temperatures))

class forestcastDisplay(Observer):
    def display(self, Temperature, Humidity, Pressure):


class DisplayElement:  # Интерфейс
    def display(self):
        raise NotImplementedError()

'''
class CurrentCondition(Observer, DisplayElement):  #
    def __init__(self):  # Инициализирует переменную
        self.s = None

    def update(self, s):  # Обнвляет переменную
        self.s = s

    def display(self):  # Выводит на экран переменную
        print(self.s)
'''

if __name__ == "__main__":
    n = 5
    currentConditions = currentConditionsDisplay()
    statistics = statisticsDisplay()
    forestcast = forestcastDisplay()
    for ob in range(n):
        register_observer(ob)