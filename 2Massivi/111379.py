n,m=map(int,input().split())
S=[]
for i in range(n):
    S.append(['.']*m)
k=1
for i in range(m):
    x=0
    y=i
    while y >= 0 and x < n :
        S[x][y]=k
        y-=1
        x+=1
        k+=1
l=0
p=0
for i in range(1,n):
    x = i
    y = m - 1
    while y>=0 and x<n :
        S[x][y]=k
        y-=1
        x+=1
        k+=1

for i in range(n):
    for j in range(m):
        if S[i][j]<10:
            print('  ',S[i][j],end='')
        elif S[i][j]<100:
            print(' ',S[i][j],end='')
        elif S[i][j]<1000:
            print('',S[i][j],end='')
        elif S[i][j]<10000:
            print(S[i][j], end='')
        else:
            print(S[i][j])
    print()
