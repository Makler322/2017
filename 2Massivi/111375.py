a=int(input())
S=[]
for i in range(a):
    S.append([0]*(i+1))
for i in range(a):
    for j in range(i+1):
        if j==0 or j==i:
            S[i][j]=1
        else:
            S[i][j]=S[i-1][j]+S[i-1][j-1]
for i in range(a):
    for j in range(i+1):
        if S[i][j]<10:
            print('    ',S[i][j],end='')
        elif S[i][j]<100:
            print('   ', S[i][j], end='')
        elif S[i][j] < 1000:
            print('  ', S[i][j], end='')
        elif S[i][j] < 10000:
            print(' ', S[i][j], end='')
        elif S[i][j] < 100000:
            print('', S[i][j], end='')
        elif S[i][j] < 1000000:
            print(S[i][j], end='')
        elif S[i][j] < 10000000:
            print(S[i][j])
    print()