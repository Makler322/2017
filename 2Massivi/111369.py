n=int(input())
S=[]
K=[]
p=0
for i in range(n):
    S.append(list(map(int,input().split())))
k=int(input())
if k>0:
    for i in range(k,n):
        K.append(S[i][i-k])
elif k<0:
    for i in range(n+k):
        K.append(S[i][i-k])
else:
    for i in range(n):
        K.append(S[i][i])
print(' '.join(map(str,K)))

