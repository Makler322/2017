def SwapColumns (A, i, j):
    for p in range(len(A)):
        A[p][i]=A[p][j]+A[p][i]
        A[p][j]=A[p][i]-A[p][j]
        A[p][i]=A[p][i]-A[p][j]
    return A
S=[]
a,b=map(int,input().split())
for i in range(a):
    S.append(list(map(int,input().split())))
I,J=map(int,input().split())
A=SwapColumns(S,I,J)
for k in range(a):
    print(' '.join(map(str,A[k])))