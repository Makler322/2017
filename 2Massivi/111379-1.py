n, m = map(int,input().split())
count = 1
mas = [[0 for i in range(m)] for i in range(n)]

for j in range(m) :
    x = 0
    y = j
    while x < n and y >= 0 :
        mas[x][y] = count
        x += 1
        y -= 1
        count += 1

for i in range(1, n) :
    x = i
    y = m - 1
    while x < n and y >= 0 :
        mas[x][y] = count
        y -= 1
        x += 1
        count += 1

for el in mas :
    for i in el :
        print(i, end=' ')
    print()