N = int(input())
maxDigit = N % 10
while N > 0:
    digit = N % 10
    if digit % 5 == 0:
        if digit > maxDigit:
            maxDigit = digit
    N = N // 10
if maxDigit == 0:
    print("NO")
else:
    print(maxDigit)

# При вводе числа 132 программа выведет 2
# Программа буде работать корректно при 555

# на третий вопрос ответа нет