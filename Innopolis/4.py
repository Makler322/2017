xa, ya, xb, yb = map(int, input().split())
if ya - yb == 0:
    A = - xb + xa
    B = 'a'
    C = 0
elif xa - xb == 0:
    A = 'a'
    B = ya - yb
    C = 0
else:
    x0 = (xa*(yb - ya)**2 + (xb - xa)*(yb - ya) * (0 - ya))/((yb - ya)**2 + (xb - xa)**2)
    y0 = ((xb - xa)*(0 - x0))/((yb - ya))
    A = (y0)
    B = (-x0)
    C = 0
n = int(input())
S = []
#print(A, B, C)
for i in range(n):
    X, Y, r = map(int, input().split())
    A1 = ya - yb
    B1 = xb - xa
    C1 = -(A1 * X + B1 * Y)
    if B == 'a':
        x, y = 0, Y
    elif A == 'a':
        x, y = X, 0
    elif A * B1 - A1 * B == 0:
        x, y = X, Y
    else:
        x = (C1 * B) / (A * B1 - A1 * B)
        y = (A * C1) / (A * B1 - A1 * B)
    S.append([x, y, r])
S = sorted(S, key = lambda item: (item[0], item[1]))
#print(S)
Ans = []
for i in range(n - 1):
    d = ((S[i][0] - S[i + 1][0])**2 + (S[i][1] - S[i + 1][1])**2)**0.5 - ((S[i][2] + (S[i + 1][2])))
    Ans.append(d)
c = max(Ans)
#print(Ans)
if c <= 0.00000021235:
    print(0)
else:
    print(c)

