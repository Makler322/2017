import collections as coll

n, m = map(int, input().split())
S = []
for i in range(n):
    sr = list(map(int, input().split()))
    S.append(sr)


def check(Sa):
    global Used, Summ, S
    if Sa[0] < 0 or Sa[0] >= n or Sa[1] < 0 or Sa[1] >= m:
        return False
    #print(int(S[Sa[0]] [Sa[1]]))
    if Summ + S[Sa[0]] [Sa[1]] < 0:
        return False
    Summ += S[Sa[0]] [Sa[1]]
    return Used[Sa[0]][Sa[1]]


for i1 in range(n):
    for j1 in range(m):
        Flag = False
        if S[i1][j1] > 0:
            Flag = True
            deck = coll.deque()
            deck.append([i1, j1])
            Summ = S[i1][j1]
            #print(deck)
            i, j = deck[0]
            k = 0
            while len(deck) > 0:
                print(1)
                Used = [[True for i in range(m)] for i in range(n)]
                Pre = [[[False, False] for i in range(m)] for i in range(n)]
                dx = [1, 0, 0, -1]
                dy = [0, 1, -1, 0]
                #print(deck[0])
                i, j = deck[0]
                for k1, k2 in zip(dx, dy):
                    if check([i + k1, j + k2]):
                        deck.append([i + k1, j + k2])
                        Used[i + k1][j + k2] = False
                        k += 1
                        Pre[i + k1][j + k2] = [i, j]
                Itog = deck[0]
                deck.popleft()
            if k == n * m:
                answer = []
                while Itog != [i1, j1]:
                    # print(Itog, [x1_start, y1_start, x2_start, y2_start], answer)
                    answer.append(Itog)
                    [i1, j1] = Itog
                    Itog = Pre[i1][j1]
