def SelectionSort(A):
    for j in range(0, len(A) - 1):
        i_max = j
        for i in range(j, len(A)):
            if A[i] < A[i_max]:
                i_max = i
        A[j],A[i_max] = A[i_max], A[j]
    return A
a=list(map(int,input().split()))
b=list(map(int,input().split()))
for j in range(0,len(a)-1):
    i_max=j
    for u in range(j,len(a)):
        if a[u]>a[i_max]:
            i_max=u
    a[j],a[i_max]=a[i_max],a[j]
b=SelectionSort(b)
S=0
for i in range (len(a)):
    S+=a[i]*b[i]
print(S)