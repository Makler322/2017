a=list(map(int,input().split()))
b=list(map(int,input().split()))
def merge(mas1,mas2):
    mas3=[]
    i=0
    j=0
    while i<=len(mas1)-1 and j<=len(mas2)-1:
        if mas1[i]<mas2[j]:
            mas3.append(mas1[i])
            i+=1
        else:
            mas3.append(mas2[j])
            j+=1
    mas3+=mas1[i:]+mas2[j:]
    return mas3
print(' '.join(map(str,merge(a,b))))