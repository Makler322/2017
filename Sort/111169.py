def merge_sort(mas1,mas2):
    mas3=[]
    i=0
    j=0
    while i<=len(mas1)-1 and j<=len(mas2)-1:
        if mas1[i]<mas2[j]:
            mas3.append(mas1[i])
            i+=1
        else:
            mas3.append(mas2[j])
            j+=1
    mas3+=mas1[i:]+mas2[j:]
    return mas3
def s_m(mas):
    mas1,mas2=mas[:len(mas)//2],mas[len(mas)//2:]
    mas1=s_m(mas1) if len(mas1)>1 else mas1
    mas2=s_m(mas2) if len(mas2)>1 else mas2
    mas3=merge_sort(mas1,mas2)
    return mas3
a=list(map(int,input().split()))
b=list(map(int,input().split()))
K=[]
L=[]
for i in range(len(a)):
    for j in range(len(b)):
        k=i+j
        l=a[i]*b[j]
        K.append(k)
        L.append(l)
P=K.copy()
P=s_m(P)
P.append(-1)
for i in range(len(P)-1):
    if P[i]!=P[i+1]:
        s=0
        for j in range(len(K)):
            if P[i]==K[j]:
                s+=L[j]
        print(s,end=' ')