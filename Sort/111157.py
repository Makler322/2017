def InsertionSort(A):
    for j in range(1, len(A)):
        for i in range(j, 0, -1):
            if A[i] < A[i - 1]:
                A[i - 1], A[i] = A[i], A[i - 1]
    return A
a=list(map(int,input().split()))
a=InsertionSort(a)
print(' '.join(map(str,a)))