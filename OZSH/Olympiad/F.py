N = int(input())
if N % 2 == 1:
    p = N // 2
    q = p + 1
elif N % 4 == 0:
    p = N//2 - 1
    q = N//2 + 1
else:
    p = N // 2 - 2
    q = N // 2 + 2
if p < 2 or q < 2:
    print(-1)
else:
    print(p, q)

