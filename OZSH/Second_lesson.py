import struct
class APP():
    def __init__(self):
        pass

    def write_data(data,file_name):
        file = open(file_name, mode ='wb')
        file.write(data)
        file.close()
        pass

    def read_data(file_name):
        file = open(file_name, mode='rb')
        file.read()
        file.close()
        pass

class object_park():
    def __init__(self):
        self.tree = 40
        self.bench = 5
        self.lamp = 12
        self.pond = 1
        self.bed = 20
        self.people = 15

    def change_people(self):
        self.people -= self.lamp - self.bench
        self.bed = self.people + 5

    def parse_data(self, data):
        self.tree, self.bench, self.lamp, self.pond, self.bed, self.people = struct.unpack('<HHHHHH',data[6:-4])
        pass

    def seriolize_park(self):
        header = b"PARK " #Заголовок в байтовой строке
        fields = struct.pack("<HHHHHH", self.tree, self.bench, self.lamp, self.pond, self.bed, self.people) #Перевод в байтовый формат
        end_file = b'eend'
        data = header + fields + end_file
        return  data