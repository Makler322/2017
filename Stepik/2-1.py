def XOR(a,b):
    a_bin = bin(a)[2:].zfill(8)
    b_bin = bin(b)[2:].zfill(8)
    c_bin = '0b'
    for i, j in zip(a_bin, b_bin):
        if i == j:
            c_bin += '0'
        else:
            c_bin += '1'
    return int(c_bin, 2)


b = int(input())
S = list(map(int, input().split()))
k = 0
if b > 1:
    X = XOR(S[0], S[1])
    for i in range(2, b):
        X = XOR(X, S[i])
else:
    X = S[0]

print(XOR(X, X), 1)