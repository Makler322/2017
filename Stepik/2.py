A, B, C, D = map(int, input().split('.'))

A_bin = str(bin(A))[2:].zfill(8)
B_bin = str(bin(B))[2:].zfill(8)
C_bin = str(bin(C))[2:].zfill(8)
D_bin = str(bin(D))[2:].zfill(8)

N = int(input())

ABBA = set()

for i in range(N):
    a, b, c, d = map(int, input().split('.'))

    a_bin = str(bin(a))[2:].zfill(8)
    #print(a_bin)
    b_bin = str(bin(b))[2:].zfill(8)
    c_bin = str(bin(c))[2:].zfill(8)
    d_bin = str(bin(d))[2:].zfill(8)

    a_new = [0] * len(a_bin)
    for ik in range(len(a_bin)):
        if a_bin[ik] == A_bin[ik] and  A_bin[ik] == '1':
            a_new[ik] = '1'
        else:
            a_new[ik] = '0'

    b_new = [0] * len(b_bin)
    for ik in range(len(b_bin)):
        if b_bin[ik] == B_bin[ik] == '1':
            b_new[ik] = '1'
        else:
            b_new[ik] = '0'

    c_new = [0] * len(c_bin)
    for ik in range(len(c_bin)):
        if c_bin[ik] == C_bin[ik] == '1':
            c_new[ik] = '1'
        else:
            c_new[ik] = '0'

    d_new = [0] * len(d_bin)
    for ik in range(len(d_bin)):
        if d_bin[ik] == D_bin[ik] == '1':
            d_new[ik] = '1'
        else:
            d_new[ik] = '0'
    Dik = (''.join(map(str, a_new)), '.', ''.join(map(str, b_new)), '.', ''.join(map(str, c_new)), '.', ''.join(map(str, d_new)))
    #print(Dik)

    ABBA.add(Dik)

print(len(ABBA))