import collections as coll

def check(S):
    global Used, Size
    for i in S:
        if i < 0 or i >= Size:
            return False
    return Used[S[0]][S[1]]


x1_start, y1_start = map(int,input().split()) #input data
x1_end, y1_end = map(int,input().split())

x1_start -= 1
y1_start -= 1
x1_end -= 1
y1_end -= 1

deck = coll.deque()
deck.append([x1_start, y1_start])




Size = 8 #Chess size
Used = [[True for i in range(Size)] for i in range(Size)]
Pre = [[[False, False] for i in range(Size)] for i in range(Size)]

dx1 = [1, 1, 1, 0, 0, 0, -1, -1, -1]
dy1 = [0, 1, -1, 0, 1, -1, 0, 1, -1]

while len(deck) != 0:
    x0_1, y0_1 = deck[0]
    for i1 in dx1:
        for i2 in dy1:
            if check([x0_1 + i1, y0_1 + i2]):
                deck.append([x0_1 + i1, y0_1 + i2])
                Used[x0_1 + i1][y0_1 + i2] = False
                Pre[x0_1 + i1][y0_1 + i2] = [x0_1, y0_1]
    deck.popleft()

Itog = [x1_end, y1_end]
k = 0
while  Itog != [x1_start, y1_start]:
    x1, y1 = Itog
    Itog = Pre[x1][y1]
    k += 1
print(k)