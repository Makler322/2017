import operator
file = open('input.txt', 'r')
answer = open('output.txt', 'w')
N, I = map(int,(file.readline().split()))
AAA, BBB, CCC = {}, {}, {}
TEAM = [0] * N
TEAMTIME = [0] * N
for i in range(1, N + 1):
    A, B, C = (list(map(int,(file.readline().split()))))

    A1 = A
    B1 = A + B
    C1 = A + B + C
    AAA[i] = A1
    BBB[i] = B1
    CCC[i] = C1

sorted_AAA = sorted(AAA.items(), key=operator.itemgetter(1))
sorted_BBB = sorted(BBB.items(), key=operator.itemgetter(1))
sorted_CCC = sorted(CCC.items(), key=operator.itemgetter(1))
sorted_AAA.reverse()
sorted_BBB.reverse()
sorted_CCC.reverse()
#print(sorted_AAA)
#print(sorted_BBB)
#print(sorted_CCC)

for i in range(N - 1):
    if sorted_AAA[i][1] > sorted_AAA[i+1][1]:
        TEAMTIME[sorted_AAA[i][0] - 1] += (N - i - 1)
        TEAM[sorted_AAA[i][0] - 1] += (N - i - 1)

    if sorted_BBB[i][1] > sorted_BBB[i+1][1]:
        TEAMTIME[sorted_BBB[i][0] - 1] += (N - i - 1)
        TEAM[sorted_BBB[i][0] - 1] += (N - i - 1)

    if sorted_CCC[i][1] > sorted_CCC[i+1][1]:
        TEAMTIME[sorted_CCC[i][0] - 1] += (N - i - 1)
        TEAM[sorted_CCC[i][0] - 1] += (N - i - 1)

#print(TEAMTIME)
#print(TEAM)
TEAMTIME.sort()
for i in range(len(TEAMTIME)):
    if TEAM[I-1] == TEAMTIME[i]:
        answer.write(str(i+1))
        break

