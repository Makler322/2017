l, n, m = map(int, input().split())
L = list(map(int, input().split()))
N = list(map(int, input().split()))
ROAD = [0] * l

def right(n):
    cost = 0
    n -= 1
    while n < len(L):
        if ROAD[n] != 0:
            return [int(cost), int(n + 1)]
        else:
            if n < len(L) - 1:
                n += 1
                cost += L[n]
            else:
                return -1

def left(n):
    cost = 0
    n -= 1
    while n > - 1:
        if ROAD[n] != 0:
            return [int(cost), int(n + 1)]
        else:
            if n > 0:
                n -= 1
                cost += L[n]

            else:
                return -1
#print(right(3), right(1), right(2))
#print(left(3), left(1), left(2))
for i in range(m):
    a, b = map(int, input().split())
    ROAD[a - 1] += b
#print(ROAD)
summ = 0
for i in range(len(N)):
    if left(N[i]) == -1:
        #print(right(N[i]))
        #print(right(N[i])[0])
        summ += right(N[i])[0]
        ROAD[right(N[i])[1] - 1] -= 1
    elif right(N[i]) == -1:
        summ += left(N[i])[0]
        ROAD[left(N[i])[1] - 1] -= 1
    else:
        if right(N[i])[0] < left(N[i])[0]:
            summ += right(N[i])[0]
            ROAD[right(N[i])[1] - 1] -= 1
        elif right(N[i])[0] > left(N[i])[0]:
            summ += left(N[i])[0]
            ROAD[left(N[i])[1] - 1] -= 1
        else:
            summ += left(N[i])[0]
            ROAD[left(N[i])[1] - 1] -= 1
print(summ)