N = int(input())
K = (N + 1) // 2
MASS = []
for i in range(N):
    MASS.append([0] * N)

for i in range(1, K + 1):  # from 1 to K 1st diag
    MASS[K - i][i - 1] = i

for i in range(K, K**2 + 1, K):
    MASS[i//K - 1][i//K - 2 + K] = i # from K to K**2 2nd diag

for i in range(K - 1):
    for j in range(N):
        if MASS[i][j] != 0:
            MASS[i + 2][j] = MASS[i][j] + K - 1

for i in range(K - 1, N - 2):
    for j in range(N):
        if MASS[i][j] != 0:
            if MASS[N - 1 - i - 2][j] != 0:
                MASS[i + 2][j] = MASS[i][j] + K - 1


for i in range(N):
    print(' '.join(map(str, MASS[i])))

